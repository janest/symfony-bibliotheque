<?php

namespace App\Repository;

use App\Entity\Book;
use App\Entity\Loan;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Loan|null find($id, $lockMode = null, $lockVersion = null)
 * @method Loan|null findOneBy(array $criteria, array $orderBy = null)
 * @method Loan[]    findAll()
 * @method Loan[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LoanRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Loan::class);
    }

    public function isBookLoaned(Book $book)
    {
        $qb = $this->createQueryBuilder('l');

        $qb
            ->select('count(l)')
            ->andWhere('l.endDate IS NULL')
            ->andWhere('l.book = :book')
            ->setParameter('book', $book)
        ;

        return $qb->getQuery()->getSingleScalarResult();
    }
}
