<?php

namespace App\Repository;

use App\Entity\Book;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Book|null find($id, $lockMode = null, $lockVersion = null)
 * @method Book|null findOneBy(array $criteria, array $orderBy = null)
 * @method Book[]    findAll()
 * @method Book[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Book::class);
    }

    public function searchQb(array $filters = [])
    {
        $qb = $this->createQueryBuilder('b');

        if (!empty($filters['title'])) {
            $qb
                ->andWhere('b.title LIKE :title')
                ->setParameter('title', '%' . $filters['title'] . '%')
            ;
        }

        if (!empty($filters['author'])) {
            $qb
                ->andWhere(':author MEMBER OF b.authors')
                ->setParameter('author', $filters['author'])
            ;
        }

        if (!empty($filters['order'])) {
            $qb->orderBy('b.' . $filters['order']);
        } else {
            $qb->orderBy('b.title');
        }

        return $qb;
    }
}
