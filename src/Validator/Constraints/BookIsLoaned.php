<?php


namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * Class BookIsLoaned
 * @package App\Validator
 *
 * @Annotation()
 */
class BookIsLoaned extends Constraint
{
    public $message = "Ce livre est en cours d'emprunt";
}
