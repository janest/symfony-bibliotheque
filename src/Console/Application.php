<?php


namespace App\Console;

use Symfony\Bundle\FrameworkBundle\Console\Application as SymfonyConsoleApplication;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


/**
 * Class Application
 * @package AppBundle\Console
 */
class Application extends SymfonyConsoleApplication
{
    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function configureIO(InputInterface $input, OutputInterface $output)
    {
        // ANSI par défaut,
        // désactivable en passant l'option --no-ansi
        // cf parent::configureIO()
        $output->setDecorated(true);

        parent::configureIO($input, $output);

        // Interactif par défaut (pour le "command line tool" de PHPStorm)
        // mais désactivable en passant l'option --no-interaction
        $interactive = !$input->hasParameterOption(array('--no-interaction', '-n'), true);

        $input->setInteractive($interactive);
    }

}
