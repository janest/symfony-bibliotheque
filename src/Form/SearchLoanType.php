<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchLoanType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'user',
                EntityType::class,
                [
                    'label' => 'Utilisateur',
                    'class' => User::class,
                    'placeholder' => 'Choisissez'
                ]
            )
            ->add(
                'book',
                EntityType::class,
                [
                    'label' => 'Livre'
                ]
            )
            ->add(
                'order',
                ChoiceType::class,
                [
                    'label' => 'Trier par',
                    'choices' => [
                        'Livre' => 'book',
                        'Utilisateur' => 'user'
                    ]
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
