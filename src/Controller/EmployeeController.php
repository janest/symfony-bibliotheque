<?php

namespace App\Controller;

use App\Entity\Employee;
use App\Form\EmployeeType;
use App\Repository\EmployeeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class EmployeeController
 * @package App\Controller
 *
 * @Route("/employe")
 */
class EmployeeController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function index(Request $request, EmployeeRepository $employeeRepository)
    {

        $employees = $employeeRepository->findBy(
            [],
            ['email' => 'ASC']
        );

        return $this->render(
            'employee/index.html.twig',
            [
                'employees' => $employees
            ]
        );
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param $id
     *
     * @Route("/nouveau")
     */
    public function create(
        Request $request,
        EntityManagerInterface $em,
        UserPasswordEncoderInterface $passwordEncoder
    ) {
        $employee = new Employee();

        $form = $this->createForm(EmployeeType::class, $employee, ['validation_groups' => ['Default', 'creation']]);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                $password = $passwordEncoder->encodePassword(
                    $employee,
                    $employee->getPlainPassword()
                );

                $employee->setPassword($password);

                $em->persist($employee);
                $em->flush();

                $this->addFlash('success', "L'employé est créé");

                return $this->redirectToRoute('app_employee_index');
            } else {
                $this->addFlash('error', 'Le formulaire contient des erreurs');
            }
        }

        return $this->render(
            'employee/create.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("/modifier/{id}", requirements={"id": "\d+"})
     */
    public function edit(
        Request $request,
        EntityManagerInterface $em,
        UserPasswordEncoderInterface $passwordEncoder,
        Employee $employee
    ) {
        $form = $this->createForm(EmployeeType::class, $employee);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                if (!empty($employee->getPlainPassword())) {
                    $password = $passwordEncoder->encodePassword(
                        $employee,
                        $employee->getPlainPassword()
                    );

                    $employee->setPassword($password);
                }

                $em->persist($employee);
                $em->flush();

                $this->addFlash('success', "L'employé est créé");

                return $this->redirectToRoute('app_employee_index');
            } else {
                $this->addFlash('error', 'Le formulaire contient des erreurs');
            }
        }

        return $this->render(
            'employee/edit.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("/suppression/{id}")
     */
    public function delete(EntityManagerInterface $em, Employee $employee)
    {

        $em->remove($employee);
        $em->flush();

        $this->addFlash('success', "L'employé est supprimé");

        return $this->redirectToRoute('app_employee_index');
    }
}
