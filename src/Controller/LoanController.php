<?php

namespace App\Controller;

use App\Entity\Loan;
use App\Form\LoanType;
use App\Repository\LoanRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LoanController
 * @package App\Controller
 *
 * @Route("/emprunts")
 */
class LoanController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function index(LoanRepository $loanRepository)
    {
        $loans = $loanRepository->findBy(
            [],
            ['startDate' => 'DESC']
        );

        return $this->render(
            'loan/index.html.twig',
            [
                'loans' => $loans
            ]
        );
    }

    /**
     * @Route("/nouveau")
     */
    public function add(Request $request, EntityManagerInterface $em)
    {
        $loan = new Loan();

        $form = $this->createForm(LoanType::class, $loan);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em->persist($loan);
                $em->flush();

                $this->addFlash('success', "L'emprunt est enregistré");

                return $this->redirectToRoute('app_loan_index');
            } else {
                $this->addFlash('error', 'Le formulaire contient des erreurs');
            }
        }

        return $this->render(
            'loan/add.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("/retour/{id}")
     */
    public function registerReturn(EntityManagerInterface $em, Loan $loan)
    {
        $loan->setEndDate(new \DateTime());

        $em->flush();

        $this->addFlash('success', 'Le livre est rendu');

        return $this->redirectToRoute('app_loan_index');
    }
}
