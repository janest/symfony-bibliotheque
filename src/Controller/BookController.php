<?php

namespace App\Controller;

use App\Entity\Book;
use App\Form\BookType;
use App\Form\SearchBookType;
use App\Repository\BookRepository;
use Doctrine\ORM\EntityManagerInterface;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BookController
 * @package App\Controller
 *
 * @Route("/livres")
 */
class BookController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function index(Request $request, BookRepository $bookRepository)
    {

        $books = $bookRepository->findBy(
            [],
            ['title' => 'ASC']
        );

        return $this->render(
            'book/index.html.twig',
            [
                'books' => $books
            ]
        );
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param $id
     *
     * @Route("/edition/{id}", defaults={"id": null}, requirements={"id": "\d+"})
     */
    public function edit(Request $request, EntityManagerInterface $em, $id)
    {
        if (is_null($id)) {
            $book = new Book();
        } else {
            $book = $em->find(Book::class, $id);

            if (is_null($book)) {
                throw new NotFoundHttpException();
            }
        }

        $form = $this->createForm(BookType::class, $book);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em->persist($book);
                $em->flush();

                $this->addFlash('success', 'Le livre est enregistré');

                return $this->redirectToRoute('app_book_index');
            } else {
                $this->addFlash('error', 'Le formulaire contient des erreurs');
            }
        }

        return $this->render(
            'book/edit.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("/suppression/{id}")
     */
    public function delete(EntityManagerInterface $em, Book $book)
    {
        $em->remove($book);
        $em->flush();

        $this->addFlash('success', 'Le livre est supprimé');

        return $this->redirectToRoute('app_book_index');
    }
}
