<?php

namespace App\Controller;

use App\Entity\Author;
use App\Form\AuthorType;
use App\Repository\AuthorRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AuthorController
 * @package App\Controller
 *
 * @Route("/auteurs")
 */
class AuthorController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function index(Request $request, AuthorRepository $authorRepository)
    {

        $authors = $authorRepository->findBy(
            [],
            ['lastname' => 'ASC', 'firstname' => 'ASC']
        );

        return $this->render(
            'author/index.html.twig',
            [
                'authors' => $authors
            ]
        );
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param $id
     *
     * @Route("/edition/{id}", defaults={"id": null}, requirements={"id": "\d+"})
     */
    public function edit(Request $request, EntityManagerInterface $em, $id)
    {
        if (is_null($id)) {
            $author = new Author();
        } else {
            $author = $em->find(Author::class, $id);

            if (is_null($author)) {
                throw new NotFoundHttpException();
            }
        }

        $form = $this->createForm(AuthorType::class, $author);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em->persist($author);
                $em->flush();

                $this->addFlash('success', "L'auteur est enregistré");

                return $this->redirectToRoute('app_author_index');
            } else {
                $this->addFlash('error', 'Le formulaire contient des erreurs');
            }
        }

        return $this->render(
            'author/edit.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("/suppression/{id}")
     */
    public function delete(EntityManagerInterface $em, Author $author)
    {

        if (!$author->getBooks()->isEmpty()) {
            $this->addFlash('error', "L'auteur ne peut pas être supprimé");
        } else {
            $em->remove($author);
            $em->flush();

            $this->addFlash('success', "L'auteur est supprimé");

        }

        return $this->redirectToRoute('app_author_index');
    }
}
